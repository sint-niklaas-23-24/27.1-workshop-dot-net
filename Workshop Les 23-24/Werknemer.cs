﻿using System.Windows.Media.Imaging;

namespace Workshop_Les_23_24
{
    internal class Werknemer : Persoon
    {
        //attri
        private double _loon;
        //constr
        public Werknemer(string eenVoornaam, string eenAchternaam, BitmapImage eenAfbeelding, double eenLoon) : base(eenVoornaam, eenAchternaam, eenAfbeelding)
        {
            Loon = eenLoon;
        }
        //Properties
        public double Loon
        {
            get { return _loon; }
            set { _loon = value; }
        }
        //Methode
        public override string Details()
        {
            return base.Details() + " €" + Loon;
        }

    }
}
