﻿using System.Windows.Media.Imaging;

namespace Workshop_Les_23_24
{
    internal class Persoon
    {
        //Attributen
        private string _voornaam;
        private string _achternaam;
        private BitmapImage _afbeelding;
        private Guid _guido;
        //Constructor
        public Persoon() { }
        public Persoon(string eenVoornaam, string eenAchternaam, BitmapImage eenAfbeelding)
        {
            Voornaam = eenVoornaam;
            Achternaam = eenAchternaam;
            Afbeelding = eenAfbeelding;
            _guido = Guid.NewGuid();

        }

        //Properties
        public string Voornaam { get { return _voornaam; } set { _voornaam = value; } }
        public string Achternaam { get { return _achternaam; } set { _achternaam = value; } }
        public BitmapImage Afbeelding { get { return _afbeelding; } set { _afbeelding = value; } }
        public Guid Guido
        {
            get { return _guido; }

        }
        //Methode
        public virtual string Details()
        {
            return Afbeelding + " " + Voornaam + " " + Achternaam;
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;

            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Persoon persoon = (Persoon)obj;
                    if (this.Guido == persoon.Guido)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
    }
}
