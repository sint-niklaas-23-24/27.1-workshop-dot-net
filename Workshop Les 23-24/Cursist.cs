﻿using System.Windows.Media.Imaging;

namespace Workshop_Les_23_24
{
    internal class Cursist : Persoon
    {
        //Attributen
        private Guid _cursistennummer;
        //Constructor
        public Cursist(string eenVoornaam, string eenAchternaam, BitmapImage eenAfbeelding) : base(eenVoornaam, eenAchternaam, eenAfbeelding)
        {
            _cursistennummer = Guid.NewGuid();
        }
        public Cursist() : base() { }
        //Properties
        public Guid Cursistennummer { get { return _cursistennummer; } }
        //Methodes
        public override string Details()
        {
            return base.Details() + " " + Cursistennummer;
        }
    }
}
