﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Workshop_Les_23_24
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        BitmapImage profielFoto = new BitmapImage();
        List<Persoon> listPersonen = new List<Persoon>();
        Persoon geselecteerdPersoon = new Persoon();
        bool Wijziging;
        private void BtnOpslagen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Wijziging = false;
                foreach (Persoon persoon in listPersonen)
                {

                    if (Equals(persoon, geselecteerdPersoon))
                    {
                        persoon.Voornaam = txtVoornaam.Text;
                        persoon.Achternaam = txtAchternaam.Text;
                        persoon.Afbeelding = profielFoto;
                        Wijziging = true;
                    }
                }
                if (Wijziging == false)
                {
                    if (!txtAchternaam.Text.Any(char.IsDigit) && !txtVoornaam.Text.Any(char.IsDigit) && !String.IsNullOrWhiteSpace(txtAchternaam.Text) && !String.IsNullOrWhiteSpace(txtVoornaam.Text))
                    {

                        MessageBox.Show("Welkom " + txtVoornaam.Text + " " + txtAchternaam.Text + " !", ".Net traject", MessageBoxButton.OK, MessageBoxImage.Information);
                        Persoon nieuwPersoon = new Persoon(txtVoornaam.Text, txtAchternaam.Text, profielFoto);
                        listPersonen.Add(nieuwPersoon);

                    }
                    else
                    {
                        MessageBox.Show("Vul alsjeblieft een geldig naam in en laat geen veld leeg. (bv: Illya Verheyden)", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                txtAchternaam.Clear();
                txtVoornaam.Clear();
                Refreshboxen();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void BtnAfbeelding_Click(object sender, RoutedEventArgs e)
        {
            string path = Environment.CurrentDirectory + "\\Fotokes";
            try
            {
                string filePath = string.Empty;
                OpenFileDialog openFileDialog1 = new OpenFileDialog
                {

                    Title = "Bestand kiezen",

                    CheckFileExists = true,
                    CheckPathExists = true,
                    InitialDirectory = Environment.CurrentDirectory + "\\Fotokes",
                    DefaultExt = "",
                    AddExtension = true,
                    FilterIndex = 4,
                    RestoreDirectory = false,

                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };
                if (openFileDialog1.ShowDialog() == true)
                {
                    profielFoto = new BitmapImage(new Uri(openFileDialog1.FileName));
                }
                else
                {
                    throw new Exception("U gaf geen avatar mee. Probeer opnieuw.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void lbDisplayMembers_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (lbDisplayMembers.SelectedIndex != -1)
            {
                geselecteerdPersoon = lbDisplayMembers.SelectedItem as Persoon;
                txtVoornaam.Text = geselecteerdPersoon.Voornaam;
                txtAchternaam.Text = geselecteerdPersoon.Achternaam;
            }
        }
        private void BtnVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            if (lbDisplayMembers.SelectedIndex != -1)
            {
                foreach (Persoon persoon in listPersonen)
                {
                    if (Equals(persoon, geselecteerdPersoon))
                    {
                        listPersonen.Remove(persoon);
                        Refreshboxen();
                        break;

                    }
                }
            }
        }
        private void Refreshboxen()
        {
            lbDisplayMembers.ItemsSource = null;
            lbDisplayMembers.ItemsSource = listPersonen;
            cmbCursistent.ItemsSource = null;
            cmbCursistent.ItemsSource = listPersonen;
        }
    }
}
